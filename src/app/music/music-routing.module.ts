import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MusicSearchViewComponent } from "./music-search-view/music-search-view.component";

const routes: Routes = [
  {
    path: "",
    component: MusicSearchViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule {}
