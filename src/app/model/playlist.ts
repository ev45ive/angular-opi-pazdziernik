// import DefaultTrackPlacki from './track';
import { Track } from './track';

export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * HEX color #ff0000
   */
  color: string;
  tracks?: Track[]
}

