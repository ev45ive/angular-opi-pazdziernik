import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules, RouterPreloader } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path:'search',
    loadChildren:'./music/music.module#MusicModule'
  },
  {
    path: "**",
    redirectTo: "search",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      // preloadingStrategy: PreloadAllModules
      // preloadingStrategy: RouterPreloader
      // useHash: true,
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
