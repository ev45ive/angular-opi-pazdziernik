import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { PlaylistComponent } from "./containers/playlist/playlist.component";
import { PlaylistsService } from "./services/playlists.service";

const routes: Routes = [
  {
    path: "playlists",
    component: PlaylistsViewComponent,
    children: [
      {
        path: ":id",
        data:{
          title: 'placki',
        },
        resolve: {
          selected: PlaylistsService
        },
        component: PlaylistComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
