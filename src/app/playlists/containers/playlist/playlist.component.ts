import { Component, OnInit } from "@angular/core";
import { PlaylistsService } from "../../services/playlists.service";
import { Playlist } from "@/model/playlist";
import { ActivatedRoute } from "@angular/router";
import { map, tap } from 'rxjs/operators';

@Component({
  selector: "app-playlist",
  templateUrl: "./playlist.component.html",
  styleUrls: ["./playlist.component.css"]
})
export class PlaylistComponent implements OnInit {
  // selected = this.service.getSelected();

  selected = this.route.data.pipe(
    tap(console.log),
    map( data => data['selected'])
  )

  constructor(
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {}

  save(draft: Playlist) {
    this.service.savePlaylist(draft);
  }

  ngOnInit() {}
}
