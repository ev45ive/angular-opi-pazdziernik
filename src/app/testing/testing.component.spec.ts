import { async, ComponentFixture, TestBed, inject } from "@angular/core/testing";

import { TestingComponent } from "./testing.component";
import { By } from "@angular/platform-browser";

fdescribe("TestingComponent", () => {
  let fixture: ComponentFixture<TestingComponent>;
  let component: TestingComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      imports: [],
      providers: [
        {
          provide: "MyService",
          useValue: { getMessage() {} }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create TestingComponent", () => {
    expect(component).toBeTruthy();
  });

  it("should render message", () => {
    expect(fixture.nativeElement.innerHTML).toMatch("testing works!");
  });

  it("should render chaged message", () => {
    component.message = "Placki!!!";
    fixture.detectChanges();

    const elem = fixture.debugElement.query(By.css(".message"));

    expect(elem.nativeElement.innerHTML).toMatch("Placki!!!");
  });

  it("should render message in input", () => {
    const elem = fixture.debugElement.query(By.css("input"));
    expect(elem.nativeElement.value).toBe(component.message);
  });

  it("should update message when input value changes", () => {
    const elem = fixture.debugElement.query(By.css("input"));
    elem.nativeElement.value = "changed message";

    // elem.nativeElement.dispatchEvent(new Event('input'))

    elem.triggerEventHandler("input", {
      target: elem.nativeElement
    });

    expect(component.message).toBe("changed message");
  });

  it("should get message form service", inject(["MyService"], (service) => {

    const spy = spyOn(service,'getMessage').and.returnValue('Placki')

    component.getMessage()

    expect(spy).toHaveBeenCalled()
    expect(component.message).toEqual('Placki')
  }));
});
