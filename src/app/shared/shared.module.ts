import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './highlight.directive';
import { UnlessDirective } from './unless.directive';
import { CardComponent } from './card/card.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tab/tab.component';
import { TabsNavComponent } from './tabs-nav/tabs-nav.component';
import { ShortenPipe } from './shorten.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  // -m shared:
  declarations: [HighlightDirective, UnlessDirective, CardComponent, TabsComponent, TabComponent, TabsNavComponent, ShortenPipe],
  // --export true:
  exports: [HighlightDirective, UnlessDirective, CardComponent, TabsComponent, TabComponent, TabsNavComponent, ShortenPipe]
})
export class SharedModule { }
