import { Component, OnInit, Input, EventEmitter } from "@angular/core";

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.css"]
})
export class TabComponent implements OnInit {

  @Input()
  title: string;

  activeChange = new EventEmitter()

  active = false;

  toggle() {
    this.active = !this.active
    this.activeChange.emit()
  }

  constructor() {}

  ngOnInit() {}
}
