import { Directive, Input, HostBinding, HostListener } from "@angular/core";

@Directive({
  selector: "[appHighlight]",
  host: {
    // '[style.color]':'color',
    // '(mouseenter)':'activate($event.x)'
  }
})
export class HighlightDirective {
  @Input()
  appHighlight: string;

  @HostBinding("style.color")
  get color(){
    return this.active? this.appHighlight : ''
  }

  active = false;

  @HostListener("mouseenter")
  activate() {
    this.active = true;
  }

  @HostListener("mouseleave")
  deactivate() {
    this.active = false;
  }

  constructor() {}
}

// console.log(HighlightDirective)